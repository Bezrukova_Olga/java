package com.company.Inheritance;

import com.company.Collections.Color;

public class Cycle extends GroundTransportation {
    private int countOfSpeed;
    private Color color;
    private boolean flashlight;

    public Cycle(int speed, int capacity, int maxSpeed, int numberOfWheels, int volumeOfTheTank, int countOfSpeed, Color color, boolean flashlight) {
        super(speed, capacity, maxSpeed, numberOfWheels, volumeOfTheTank);
        this.countOfSpeed = countOfSpeed;
        this.color = color;
        this.flashlight = flashlight;
    }

    public int getCountOfSpeed() {
        return countOfSpeed;
    }

    public void setCountOfSpeed(int countOfSpeed) {
        this.countOfSpeed = countOfSpeed;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public boolean isFlashlight() {
        return flashlight;
    }

    public void setFlashlight(boolean flashlight) {
        this.flashlight = flashlight;
    }
}
