package com.company.Inheritance;

public class Helicopter extends AirTransport {

    private int numberOfScrews;

    public Helicopter(int speed, int capacity, int maxSpeed, String chassisType, int numberOfScrews) {
        super(speed, capacity, maxSpeed, chassisType);
        this.numberOfScrews = numberOfScrews;
    }

    public int getNumberOfScrews() {
        return numberOfScrews;
    }

    public void setNumberOfScrews(int numberOfScrews) {
        this.numberOfScrews = numberOfScrews;
    }
}
