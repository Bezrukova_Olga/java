package com.company.Inheritance;

public class Airplane extends AirTransport {

    private int maximumHeight;
    private String type;

    public Airplane(int speed, int capacity, int maxSpeed, String chassisType, int maximumHeight, String type) {
        super(speed, capacity, maxSpeed, chassisType);
        this.maximumHeight = maximumHeight;
        this.type = type;
    }

    public int getMaximumHeight() {
        return maximumHeight;
    }

    public void setMaximumHeight(int maximumHeight) {
        this.maximumHeight = maximumHeight;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
