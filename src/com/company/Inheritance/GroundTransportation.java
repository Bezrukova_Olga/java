package com.company.Inheritance;

public class GroundTransportation extends Transport {
    private int numberOfWheels; //количество колес
    private int volumeOfTheTank; //вместимость бензобака

    public GroundTransportation(int speed, int capacity, int maxSpeed, int numberOfWheels, int volumeOfTheTank) {
        super(speed, capacity, maxSpeed);
        this.numberOfWheels = numberOfWheels;
        this.volumeOfTheTank = volumeOfTheTank;
    }

    @Override
    public void sound() {
        System.out.println("Bip-bip");
    }

    public void takeAwayGasoline(int sub) { //уменьшить количество бензина в баке
        this.volumeOfTheTank -= sub;
    }

    public int getNumberOfWheels() {
        return numberOfWheels;
    }

    public void setNumberOfWheels(int numberOfWheels) {
        this.numberOfWheels = numberOfWheels;
    }

    public int getVolumeOfTheTank() {
        return volumeOfTheTank;
    }

    public void setVolumeOfTheTank(int volumeOfTheTank) {
        this.volumeOfTheTank = volumeOfTheTank;
    }
}
