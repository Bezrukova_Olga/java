package com.company.Inheritance;

public class AirTransport extends Transport {
    private String chassisType;

    public AirTransport(int speed, int capacity, int maxSpeed, String chassisType) {
        super(speed, capacity, maxSpeed);
        this.chassisType = chassisType;
    }

    @Override
    public void sound() {
        System.out.println("Tuuu!");
    }

    @Override
    public void addSpeed(int speedToAdd) {
        super.addSpeed(speedToAdd);
    }

    public String getChassisType() {
        return chassisType;
    }

    public void setChassisType(String chassisType) {
        this.chassisType = chassisType;
    }
}
