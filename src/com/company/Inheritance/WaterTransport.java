package com.company.Inheritance;

public class WaterTransport extends Transport {

    private int power;

    public WaterTransport(int speed, int capacity, int maxSpeed, int power) {
        super(speed, capacity, maxSpeed);
        this.power = power;
    }

    @Override
    public void sound() {
        System.out.println("Biiiiip-biiiiip");
    }


}
