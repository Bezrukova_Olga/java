package com.company.Inheritance;

import com.company.Collections.Color;

public class Boat extends WaterTransport {

    private Color color;
    private String type;

    public Boat(int speed, int capacity, int maxSpeed, int power, Color color, String type) {
        super(speed, capacity, maxSpeed, power);
        this.color = color;
        this.type = type;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
