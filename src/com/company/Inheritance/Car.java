package com.company.Inheritance;

import com.company.Collections.Color;

public class Car extends GroundTransportation {
    private Color color;
    private String brand;

    public Car(int speed, int capacity, int maxSpeed, int numberOfWheels, int volumeOfTheTank, Color color, String brand) {
        super(speed, capacity, maxSpeed, numberOfWheels, volumeOfTheTank);
        this.color = color;
        this.brand = brand;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }
}
