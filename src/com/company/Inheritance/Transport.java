package com.company.Inheritance;

public class Transport {
    private int speed;
    private int capacity;
    private int maxSpeed;


    public Transport(int speed, int capacity, int maxSpeed) {
        this.speed = speed;
        this.capacity = capacity;
        this.maxSpeed = maxSpeed;
    }

    public void sound() {
        System.out.println("Transport sound");
    }

    public void addSpeed(int speedToAdd) {
        if (speed + speedToAdd < maxSpeed)
            this.speed += speedToAdd;
        else
            this.speed = maxSpeed;
    }

    public void subSpeed(int speedToSub) {
        if (speed - speedToSub >= 0)
            this.speed -= speedToSub;
        else
            this.speed = 0;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }
}
