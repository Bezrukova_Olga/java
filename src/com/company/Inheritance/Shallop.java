package com.company.Inheritance;

public class Shallop extends WaterTransport{
    private String brand;
    private String motor;

    public Shallop(int speed, int capacity, int maxSpeed, int power, String brand, String motor) {
        super(speed, capacity, maxSpeed, power);
        this.brand = brand;
        this.motor = motor;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getMotor() {
        return motor;
    }

    public void setMotor(String motor) {
        this.motor = motor;
    }
}
