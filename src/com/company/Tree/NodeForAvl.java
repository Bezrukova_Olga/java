package com.company.Tree;

public class NodeForAvl {
    int key;
    int height;
    NodeForAvl left;
    NodeForAvl right;

    NodeForAvl(int key) {
        this.key = key;
    }

    @Override
    public String toString() {
        return "NodeForAvl{" +
                "key=" + key +
                ", height=" + height +
                ", left=" + left +
                ", right=" + right +
                '}';
    }
}
