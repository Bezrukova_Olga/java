package com.company.Tree;

public class Binary {
    Node root;

    public void add(int value) {
        root = addRecursiveInTree(root, value);
    }

    private Node addRecursiveInTree(Node current, int value) {

        if (current == null) { //если текущий узел пуст, то создаем новый и присваиваем ему значение
            return new Node(value);
        }

        if (value < current.value) { // если переданное значение value меньше чем значение текущего элемента(то есть корня), то идем в левую ветку
            current.left = addRecursiveInTree(current.left, value); // рекурсивно вызываем текущий метод
        } else if (value > current.value) { // если переданное значение value больше чем значение текущего элемента(то есть корня), то идем в правую ветку
            current.right = addRecursiveInTree(current.right, value); // рекурсивно вызываем текущий метод
        }

        return current;
    }

    public boolean isEmpty() { // проверка дерева на пустоту
        return root == null;
    }


    public int getSize() { // получение размера дерева (количества элементов в нем)
        return getSizeRecursiveInTree(root);
    }

    private int getSizeRecursiveInTree(Node current) { // обход левой ветки и подсчет количества элементов, добавляем единицу (корень), обход правой ветки и подсчет количества элементов
        return current == null ? 0 : getSizeRecursiveInTree(current.left) + 1 + getSizeRecursiveInTree(current.right);
    }


    public void delete(int value) { //удаление элемента по значению
        root = deleteRecursiveInTree(root, value);
    }

    private Node deleteRecursiveInTree(Node current, int value) {
        if (current == null) { // если корень пуст, то говорим, что удалять нечего и возвращаем null
            return null;
        }

        if (value == current.value) { // если значение, которое мы ищем равняется текущему значению(на старте root),
            // то рассматриваем различные варианты
            if (current.left == null && current.right == null) { // если нет потомков, то просто возвращаем null
                return null;
            }

            if (current.right == null) { // если есть только левый потомок, то вернем его
                return current.left;
            }

            if (current.left == null) { // если есть только правый потомок, то вернем его
                return current.right;
            }
            // если имеется и левая ветка и правая,
            // то находим и удаляем потомков удаляемого элемента, а затем и сам элемент
            int smallestValue = findMinValue(current.right);
            current.value = smallestValue;
            current.right = deleteRecursiveInTree(current.right, smallestValue);
            return current;
        }
        if (value < current.value) {
            current.left = deleteRecursiveInTree(current.left, value);
            return current;
        }

        current.right = deleteRecursiveInTree(current.right, value);
        return current;
    }

    private int findMinValue(Node root) {
        return root.left == null ? root.value : findMinValue(root.left);
    }


    // различные обходы бинарного дерева
    public void inOrder(Node node) { // обход дерева: левая ветка->корень->правая ветка
        if (node != null) {
            inOrder(node.left);
            visit(node.value);
            inOrder(node.right);
        }
    }

    public void preOrder(Node node) { // обход дерева: корень->левая ветка->правая ветка
        if (node != null) {
            visit(node.value);
            preOrder(node.left);
            preOrder(node.right);
        }
    }

    public void postOrder(Node node) { // обход дерева: левая ветка->правая ветка->корень
        if (node != null) {
            postOrder(node.left);
            postOrder(node.right);
            visit(node.value);
        }
    }

    private void visit(int value) {
        System.out.print(" " + value);
    }

}
