package com.company.Tree;

public class Trie {
    private NodeForTrie root;

    Trie() { // конструктор для инициализации корня
        root = new NodeForTrie();
    }

    void insert(String word) { // вставка слова в дерево
        NodeForTrie current = root;

        for (int i = 0; i < word.length(); i++) {
            current = current.getChildren().computeIfAbsent(word.charAt(i), create -> new NodeForTrie());
        }
        current.setEndOfWord(true);
    }

    boolean delete(String word) { // удаление слова
        return delete(root, word, 0);
    }

    boolean containsNode(String word) { // проверка на наличие слова в дереве
        NodeForTrie current = root; // начинаем с корня

        for (int i = 0; i < word.length(); i++) { //пробегаемся по слову и ищем соответствие
            char ch = word.charAt(i);
            NodeForTrie node = current.getChildren().get(ch);
            if (node == null) {
                return false;
            }
            current = node;
        }
        return current.getEndOfWord();
    }

    boolean isEmpty() {
        return root == null;
    } // проверка на пустоту дерева

    private boolean delete(NodeForTrie current, String word, int index) { // удаление слова из дерева
        if (index == word.length()) {
            if (!current.getEndOfWord()) {
                return false;
            }
            current.setEndOfWord(false);
            return current.getChildren().isEmpty();
        }
        char ch = word.charAt(index);
        NodeForTrie node = current.getChildren().get(ch);
        if (node == null) {
            return false;
        }
        boolean shouldDeleteCurrentNode = delete(node, word, index + 1) && !node.getEndOfWord();

        if (shouldDeleteCurrentNode) {
            current.getChildren().remove(ch);
            return current.getChildren().isEmpty();
        }
        return false;
    }

    @Override
    public String toString() {
        return "Trie{" +
                "root=" + root +
                '}';
    }
}
