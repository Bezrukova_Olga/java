package com.company.Tree;

// АВЛ-дерево — сбалансированное по высоте двоичное дерево поиска:
// для каждой его вершины высота её двух поддеревьев различается не более чем на 1
public class Avl {

    private NodeForAvl root;

    public NodeForAvl find(int key) { // находим элемент по ключу
        NodeForAvl current = root; // начинаем с корня
        while (current != null) { // начинаем поиск, если дерево непустое
            if (current.key == key) { //если нашли, то выходим поиска
                break;
            }
            current = current.key < key ? current.right : current.left; //выбираем ветку
        }
        return current;
    }

    public void insert(int key) { // вставка элемента
        root = insert(root, key);
    }
    private NodeForAvl insert(NodeForAvl nodeForAvl, int key) { // непосредственно функция, реализующая вставку элемента
        if (nodeForAvl == null) { // если пустое дерево, то просто создаем элемент
            return new NodeForAvl(key);
        } else if (nodeForAvl.key > key) { // если значение в дереве больше переданного значения, то движемся налево
            nodeForAvl.left = insert(nodeForAvl.left, key);
        } else if (nodeForAvl.key < key) { // если значение в дереве меньше переданного значения, то движемся направо
            nodeForAvl.right = insert(nodeForAvl.right, key);
        } else { // иначе говорим, что такое значение уже было
            System.out.println("The key is repeated!");
        }
        return rebalance(nodeForAvl); // производим балансировку дерева
    }

    public void delete(int key) { // удаление элемента
        root = delete(root, key);
    }
    private NodeForAvl delete(NodeForAvl nodeForAvl, int key) {
        if (nodeForAvl == null) {
            return nodeForAvl;
        } else if (nodeForAvl.key > key) {
            nodeForAvl.left = delete(nodeForAvl.left, key);
        } else if (nodeForAvl.key < key) {
            nodeForAvl.right = delete(nodeForAvl.right, key);
        } else {
            if (nodeForAvl.left == null || nodeForAvl.right == null) {
                nodeForAvl = (nodeForAvl.left == null) ? nodeForAvl.right : nodeForAvl.left;
            } else {
                NodeForAvl mostLeftChild = mostLeftChild(nodeForAvl.right);
                nodeForAvl.key = mostLeftChild.key;
                nodeForAvl.right = delete(nodeForAvl.right, nodeForAvl.key);
            }
        }
        if (nodeForAvl != null) {
            nodeForAvl = rebalance(nodeForAvl);
        }
        return nodeForAvl;
    }

    private NodeForAvl rebalance(NodeForAvl rotate) { // балансировка дерева
        updateHeight(rotate);
        int balance = getBalance(rotate);
        if (balance > 1) {
            if (height(rotate.right.right) > height(rotate.right.left)) { // смотрим чья высота больше и исходя из этого
                // выбираем необходимый необходимые действия
                rotate = left(rotate);
            } else {
                rotate.right = right(rotate.right);
                rotate = left(rotate);
            }
        } else if (balance < -1) {
            if (height(rotate.left.left) > height(rotate.left.right)) {
                rotate = right(rotate);
            } else {
                rotate.left = left(rotate.left);
                rotate = right(rotate);
            }
        }
        return rotate;
    }
    private void updateHeight(NodeForAvl n) {
        n.height = 1 + Math.max(height(n.left), height(n.right));
    }

    public NodeForAvl getRoot() { // получаем корень (обычный геттер)
        return root;
    }

    public int height() { // получаем высоту дерева
        return root == null ? -1 : root.height;
    }

    private NodeForAvl mostLeftChild(NodeForAvl nodeForAvl) { // находим самого левого потомка
        NodeForAvl current = nodeForAvl;
        while (current.left != null) {
            current = current.left;
        }
        return current;
    }



    private NodeForAvl right(NodeForAvl rotateY) { // выполняется правый поворот (необходим для балансировки)
        NodeForAvl x = rotateY.left;
        NodeForAvl z = x.right;
        x.right = rotateY;
        rotateY.left = z;
        updateHeight(rotateY);
        updateHeight(x);
        return x;
    }

    private NodeForAvl left(NodeForAvl rotateY) { // выполняется левый поворот (необходим для балансировки)
        NodeForAvl x = rotateY.right;
        NodeForAvl z = x.left;
        x.left = rotateY;
        rotateY.right = z;
        updateHeight(rotateY);
        updateHeight(x);
        return x;
    }


    private int height(NodeForAvl n) {
        return n == null ? -1 : n.height;
    }

    public int getBalance(NodeForAvl n) {
        return (n == null) ? 0 : height(n.right) - height(n.left);
    }

    @Override
    public String toString() {
        return "Avl{" +
                "root=" + root +
                '}';
    }
}
