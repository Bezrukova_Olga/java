package com.company.Tree;

public class Main {
    public static void main(String[] args) {
        // работа с бинарным деревом
        Binary binaryTree = new Binary();
        binaryTree.add(1);
        binaryTree.add(6);
        binaryTree.add(9);
        binaryTree.add(3);
        binaryTree.add(5);
        binaryTree.add(7);
        binaryTree.add(11);
        System.out.println("Binary size = " + binaryTree.getSize());
        // различные типы обхода дерева
        binaryTree.preOrder(binaryTree.root);
        System.out.println();
        binaryTree.inOrder(binaryTree.root);
        System.out.println();
        binaryTree.postOrder(binaryTree.root);
        System.out.println();
        // удаление элемента со значением 5
        binaryTree.delete(5);
        // смотрим, что действительно элемент удален
        System.out.println("Binary size = " + binaryTree.getSize());
        // проверяем действительно ли со значением 5
        binaryTree.preOrder(binaryTree.root);
        System.out.println();


        //работа с авл деревом
        Avl avl = new Avl();
        avl.insert(1);
        avl.insert(5);
        avl.insert(6);
        avl.insert(2);
        avl.insert(9);
        avl.insert(8);
        avl.insert(3);
        // балансировка выполняется по завершении вставки элемента в дерево
        // вывод дерева
        System.out.println(avl.toString());
        // вывод элемента со значением 8
        System.out.println(avl.find(8));
        // высота дерева
        System.out.println(avl.height());

        // работа с трай деревом
        Trie trie = new Trie();
        //добавление слов
        trie.insert("Voronezh");
        trie.insert("Vladimir");
        trie.insert("Class");
        trie.insert("Oleg");
        trie.insert("Olga");
        System.out.println(trie.toString()); // структура вывода следующая:
        // буква-> потомок(следующая буква) и тд, до конца слова
        // если слова начинаются на одну и ту же букву (как Владимир и Воронеж), то от буквы В будет идти расслаивание по разным веткам
        // если совпадающих букв больше, то соответственно расслоение будет идти от первой несовпадающей буквы (например, как Ольга и Олег: Ол
        // общее, остальные буквы будут по разным веткам
        System.out.println("Find word? - " + trie.containsNode("Vladimir")); // проверка есть ли такое слово
    }
}
