package com.company.Tree;

import java.util.HashMap;
import java.util.Map;

public class NodeForTrie {

    private final Map<Character, NodeForTrie> children = new HashMap<>(); // создаем мапу в качестве узла
    private boolean endOfWord;

    Map<Character, NodeForTrie> getChildren() {
        return children;
    }

    boolean getEndOfWord() { //метод для проверки на конец слова
        return endOfWord;
    }

    void setEndOfWord(boolean endOfWord) {
        this.endOfWord = endOfWord;
    }

    @Override
    public String toString() {
        return "NodeForTrie{" +
                "children=" + children +
                ", endOfWord=" + endOfWord +
                '}';
    }
}
