package com.company.Collections;

import java.util.*;

/**
 * Создан класс машина, который хранит два значения: цвет (который представляет собой enum) и бренд
 * В данном классе рассмотрены основные типы коллекций: List (ArrayList, LinkedList), Map и Set
 * 1) при работе с arrayList были рассмотрены основные методы: add, remove. После каждого изменения листа он выводится на экран.
 * 2) при работе с Map<Integer, String> было рассмотрено: проход по map, получение размерности, получение списка ключей и значений.
 * 3) при работе с Set<Integer> было рассмотрено добавление новых элементов и показано, что TreeSet хранит не просто уникальные объекты,
 * но и в правильном порядке.
 * 4) при работе с linkedList было рассмотрено добавление элементов в лист, а также добавление нескольких элементов по индексу в список.
 */

public class MainCollections {
    public static void main(String[] args) {
        List<Car> cars = new ArrayList<>();
        cars.add(new Car(Color.WHITE, "Hyundai"));
        cars.add(new Car(Color.BLACK, "Renault"));
        cars.add(new Car(Color.BLUE, "Volkswagen"));
        System.out.println(cars.toString());
        System.out.println(cars.get(1).toString());
        cars.remove(2);
        System.out.println(cars.toString());

        System.out.println("________________________________________________________________________________________________________");
        System.out.println();

        Map<Integer, Car> carsMap = new HashMap<>();
        carsMap.put(1, new Car(Color.WHITE, "Hyundai"));
        carsMap.put(2, new Car(Color.BLACK, "Renault"));
        carsMap.put(3, new Car(Color.BLUE, "Volkswagen"));

        for (Map.Entry<Integer, Car> entry : carsMap.entrySet()) {
            System.out.println("ID = " + entry.getKey() + " Car color = " + entry.getValue().getColor() + " Car brand = " + entry.getValue().getBrand());
        }

        System.out.println("CarsMap size = " + carsMap.size());

        System.out.println(carsMap.keySet().toString());
        System.out.println(carsMap.values().toString());

        System.out.println("________________________________________________________________________________________________________");
        System.out.println();

        Set<Integer> treeSet = new TreeSet<>();

        treeSet.add(3);
        treeSet.add(2);
        treeSet.add(5);
        treeSet.add(1);
        treeSet.add(4);
        treeSet.add(5);

        System.out.println("TreeSet sorted elements and save unic objects: " + treeSet.toString());

        treeSet.remove(3);

        System.out.println("TreeSet before remove object: " + treeSet.toString());

        System.out.println("________________________________________________________________________________________________________");
        System.out.println();

        List<Car> carsLinkedList = new LinkedList<>();
        carsLinkedList.add(new Car(Color.YELLOW, "Hyundai"));
        carsLinkedList.add(new Car(Color.WHITE, "Renault"));
        carsLinkedList.add(new Car(Color.RED, "Volkswagen"));

        System.out.println("Start initial: " + carsLinkedList.toString());
        carsLinkedList.add(1, new Car(Color.BLACK, "Lexus"));
        carsLinkedList.add(1, new Car(Color.RED, "Opel"));
        System.out.println("Before changes: " + carsLinkedList.toString());

        System.out.println("Get element by index: " + carsLinkedList.get(4));
    }
}
