package com.company.Collections;

public class Car {
    private Color color;
    private String brand;

    public Car() {
    }

    public Car(Color color, String brand) {
        this.color = color;
        this.brand = brand;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    @Override
    public String toString() {
        return "Car{" +
                "color=" + color +
                ", brand='" + brand + '\'' +
                '}';
    }
}
